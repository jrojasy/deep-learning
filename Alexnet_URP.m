categories = {'Andres','Luis','Juan','Maria'};

rootFile = 'rostros';

% CONSULTA PARA TOMAR FOTOS Y AGREGARLAS AL DATA STORE
question_1 = '¿Desea tomar fotos desde la webcam y agregarlas al aprendizaje? (s/n) ';
answer_1 = input(question_1,'s');

if strcmp(answer_1, 's') 

	% CAPTURA DE IMAGENES POR WEBCAM
	cam=webcam;
	nombre = 'Ingrese su nombre: ';
	picture_name = input(nombre, 's');
	[status, msg, msgID] = mkdir (strcat(rootFile, '/', picture_name));
	
	if strcmp(msg, 'Directory already exists.')
		answer_2 = input('El directorio ya existe, ¿desea sobrescribirlo? (s/n) ', 's');
		if strcmp(answer_2,'s')
			% ELIMINACION DE DIRECTORIO ANTERIOR
			[status, message, messageid] = rmdir(strcat(rootFile, '/', picture_name), 's');
			% CREACION DEL NUEVO DIRECTORIO
			mkdir (strcat(rootFile, '/', picture_name));
			% TOMA DE FOTOS 
			disp('SE TOMARAN LAS FOTOS EN: ')
			for i=3:-1:1
				pause(0.5)
				disp(['		', num2str(i), '...'])
			end		
			for i = 1:30         
				img=snapshot(cam);
				imwrite( img , strcat(rootFile, '/', picture_name, '/img', num2str(i)  ,'.jpg'));
				pause(0.2)
			end
			disp('FOTOS TOMADAS CON EXITO.')
			% ACTUALIZA LA LISTA DE CATEGORIAS
			categories = [categories picture_name];
		else
			disp('SE UTILIZARA EL DIRECTORIO EXISTENTE.')
			% ACTUALIZA LA LISTA DE CATEGORIAS
			categories = [categories picture_name];
		end
	else
		% TOMA DE FOTOS 
		disp('SE TOMARAN LAS FOTOS EN: ')
		for i=3:-1:1
			pause(0.5)
			disp(['		', num2str(i), '...'])
		end		
		for i = 1:30         
			img=snapshot(cam);
			imwrite( img , strcat(rootFile, '/', picture_name, '/img', num2str(i)  ,'.jpg'));
			pause(0.2)
		end
		disp('FOTOS TOMADAS CON EXITO.')
		% ACTUALIZA LA LISTA DE CATEGORIAS
		categories = [categories picture_name];
	end
	clearvars cam
	% --------------------------------------
else 
	disp('SE USARAN LAS CARPETAS SIGUIENTES: ')
	cant_var = size(categories);
	for i = 1:cant_var(2)
		disp(categories(i))
	end
	clearvars cant_var
end
clearvars question_1 answer_1 
% ----------------------------------------------------------

% CREACION DE DATA STORE
[m, num_categories] = size(categories);
imds = imageDatastore(fullfile(rootFile,categories),...
    'LabelSource','foldernames');

inputSize = [227 227 3];

[imgTrain,imgTest] = splitEachLabel(imds,0.8,'randomized');
augimgTrain = augmentedImageDatastore(inputSize,imgTrain);
%-----------------------
 
 layers_1 = [
    imageInputLayer([227 227 3],"Name","data")
    convolution2dLayer([11 11],96,"Name","conv1","BiasLearnRateFactor",2,"Stride",[4 4])
    reluLayer("Name","relu1")
    crossChannelNormalizationLayer(4,"Name","norm1","K",1)
    maxPooling2dLayer([3 3],"Name","pool1","Stride",[2 2])
    groupedConvolution2dLayer([5 5],128,2,"Name","conv2","BiasLearnRateFactor",2,"Padding",[2 2 2 2])
    reluLayer("Name","relu2")
    crossChannelNormalizationLayer(4,"Name","norm2","K",1)
    maxPooling2dLayer([3 3],"Name","pool2","Stride",[2 2])
    convolution2dLayer([3 3],384,"Name","conv3","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
    reluLayer("Name","relu3")
    groupedConvolution2dLayer([3 3],192,2,"Name","conv4","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
    reluLayer("Name","relu4")
    groupedConvolution2dLayer([3 3],128,2,"Name","conv5","BiasLearnRateFactor",2,"Padding",[1 1 1 1])
    reluLayer("Name","relu5")
    maxPooling2dLayer([3 3],"Name","pool5","Stride",[2 2])
    fullyConnectedLayer(4096,"Name","fc6","BiasLearnRateFactor",2)
    reluLayer("Name","relu6")
    dropoutLayer(0.5,"Name","drop6")
    fullyConnectedLayer(4096,"Name","fc7","BiasLearnRateFactor",2)
    reluLayer("Name","relu7")
    dropoutLayer(0.5,"Name","drop7")
    fullyConnectedLayer(4,"Name","fc","BiasLearnRateFactor",2)
    softmaxLayer("Name","prob")
    classificationLayer("Name","classoutput")];

% Modificador de opciones
%opts = trainingOptions('sgdm', ... % Utiliza el descenso de gradiente estocástico conimpulso para el entrenamiento de la red
    %'InitialLearnRate', 0.001, ... % Rango inicial de aprendizaje en "0.001"
    %'ValidationData',augimgTrain, ...
    %'ValidationFrequency',3, ...
    %'LearnRateSchedule', 'piecewise', ... 	% Aprende la frecuencia de aciertos y el input correcto
    %'LearnRateDropFactor', 0.1, ...	% La tasa de aprendizaje se reduce 0.1 cada 8 épocas
    %'LearnRateDropPeriod', 8, ...
    %'L2Regularization', 0.004, ...
    %'MaxEpochs', 20, ...	% Cantidad máxima de épocas
    %'MiniBatchSize', 100, ...	% Mini lote con 100 observaciones en cada iteración
    %'Plot', 'training-progress','Verbose', true);

opts = trainingOptions('sgdm', ... % Utiliza el descenso de gradiente estocastico conimpulso para el entrenamiento de la red
    'InitialLearnRate', 0.0001, ... % Rango inicial de aprendizaje en "0.001"
    'ValidationData',augimgTrain, ...
    'ValidationFrequency',1, ...
    'MaxEpochs', 15, ...	% Cantidad maxima de epoca
    'MiniBatchSize', 64, ...	% Mini lote con [x] observaciones en cada iteracion
    'L2Regularization', 0.003, ...
    'Plot', 'training-progress','Verbose', true);

disp('ENTRENANDO RED...')

[net,info] = trainNetwork(augimgTrain,layers_1,opts);

disp('RED ENTRENADA.')
% ------------ CONSULTAR SI DESEA TOMAR FOTO ------------

consulta = 'Desea tomar una foto para el testing? (s/n) : ';
respuesta = input(consulta, 's');

if strcmp(respuesta, 's')
    % TOMAR FOTO CON LA WEBCAM Y GUARDARLA PARA EL TESTING
	cam=webcam;
	disp('LA FOTO SE TOMARA EN: ')
	for i=3:-1:1
		pause(0.5)
		disp(['		', num2str(i), '...'])
	end	   
    img=snapshot(cam);
    imwrite( img , strcat('INPUT.jpg'));
	
	disp('FOTO TOMADA CON EXITO.')
    
    %TESTEAR LA IMAGEN
    fotoTest = imageDatastore(fullfile('INPUT.jpg'),...
    'LabelSource','foldernames');
    augfotoTest = augmentedImageDatastore(inputSize,fotoTest);
    [YPred,probs] = classify(net,augfotoTest);
    
    subplot(1,1,1)
    I = readimage(fotoTest,1);
    imshow(I)
    label = YPred(1);
    clearvars max
    [maximo,pos] = max(probs);
	% ASIGNACION DE COLOR AL TITULO DE LA IMAGEN SEGUN EL ACIERTO
    if  maximo >= 0.999
        colorText = 'g';
        title( string(label) + ", " + num2str((100*maximo),3) + "%", 'Color', colorText);
    else
        colorText = 'r';
        title("IMAGEN NO RECONOCIDA - " + string(label) + ", " + num2str((100*maximo),3) + "%", 'Color', colorText);
    end
	clearvars cam;
else
    % USO DEL DATA STORE DE TESTEO
    
    % Clasificacion de imagenes prueba y calculo de precision de la clasificacion
    augimgTest = augmentedImageDatastore(inputSize,imgTest);
    [YPred,probs] = classify(net,augimgTest);
    
    % MUESTRA LA CANTIDAD DE IMAGENES DEL TESTEO
    [a, b] = size(imgTest.Files);
    for i = 1:(a)
        subplot(9,6,i)
        I = readimage(imgTest,i);
        imshow(I)
        title('Persona: N° ' + string(i));
    end
    %%%%%

    % Ingreso de imagen a probar
    prompt = 'Ingrese el número de imagen que desea probar: ';
    x = input(prompt);
    x = double(x);

    while( x > size(imgTest.Files) & x <= 0)
        x = input(prompt);
        x = double(x);
    end
    
    subplot(1,1,1)
    I = readimage(imgTest,x);
    imshow(I)
    label = YPred(x);
    clearvars max
	[maximo2,pos2] = max(probs(x,:));
    
	% ASIGNACION DE COLOR AL TITULO DE LA IMAGEN SEGUN EL ACIERTO	
    if  maximo2 >= 0.8
        colorText = 'g';
        title(string(label) + ", " + num2str((100*maximo2),3) + "%", 'Color', colorText);
    else
        colorText = 'r';
        title("IMAGEN NO RECONOCIDA - " + string(label) + ", " + num2str((100*maximo2),3) + "%", 'Color', colorText);
    end 
end

% ------------------------------------------------------------