categories = {'Andres','Luis','Juan','Maria'};
rootFile = 'rostros';

% CONSULTA PARA TOMAR FOTOS Y AGREGARLAS AL DATA STORE
question_1 = '¿Desea tomar fotos desde la webcam y agregarlas al aprendizaje? (s/n) ';
answer_1 = input(question_1,'s');

if strcmp(answer_1, 's') 

	% CAPTURA DE IMAGENES POR WEBCAM
	cam=webcam;
	nombre = 'Ingrese su nombre: ';
	picture_name = input(nombre, 's');
	[status, msg, msgID] = mkdir (strcat(rootFile, '/', picture_name));
	
	if strcmp(msg, 'Directory already exists.')
		answer_2 = input('El directorio ya existe, ¿desea sobrescribirlo? (s/n) ', 's');
		if strcmp(answer_2,'s')
			% ELIMINACION DE DIRECTORIO ANTERIOR
			[status, message, messageid] = rmdir(strcat(rootFile, '/', picture_name), 's');
			% CREACION DEL NUEVO DIRECTORIO
			mkdir (strcat(rootFile, '/', picture_name));
			% TOMA DE FOTOS 
			disp('SE TOMARAN LAS FOTOS EN: ')
			for i=3:-1:1
				pause(0.5)
				disp(['		', num2str(i), '...'])
			end		
			for i = 1:30         
				img=snapshot(cam);
				imwrite( img , strcat(rootFile, '/', picture_name, '/img', num2str(i)  ,'.jpg'));
				pause(0.2)
			end
			disp('FOTOS TOMADAS CON EXITO.')
			% ACTUALIZA LA LISTA DE CATEGORIAS
			categories = [categories picture_name];
		else
			disp('SE UTILIZARA EL DIRECTORIO EXISTENTE.')
			% ACTUALIZA LA LISTA DE CATEGORIAS
			categories = [categories picture_name];
		end
	else
		% TOMA DE FOTOS 
		disp('SE TOMARAN LAS FOTOS EN: ')
		for i=3:-1:1
			pause(0.5)
			disp(['		', num2str(i), '...'])
		end		
		for i = 1:30         
			img=snapshot(cam);
			imwrite( img , strcat(rootFile, '/', picture_name, '/img', num2str(i)  ,'.jpg'));
			pause(0.2)
		end
		disp('FOTOS TOMADAS CON EXITO.')
		% ACTUALIZA LA LISTA DE CATEGORIAS
		categories = [categories picture_name];
	end
	clearvars cam
	% --------------------------------------
else 
	disp('SE USARAN LAS CARPETAS SIGUIENTES: ')
	cant_var = size(categories);
	for i = 1:cant_var(2)
		disp(categories(i))
	end
	clearvars cant_var
end
clearvars question_1 answer_1 
% ----------------------------------------------------------

% CREACION DE DATA STORE
[m, num_categories] = size(categories);
imds = imageDatastore(fullfile(rootFile,categories),...
    'LabelSource','foldernames');

inputSize = [128 128 3];

[imgTrain,imgTest] = splitEachLabel(imds,0.8,'randomized');
augimgTrain = augmentedImageDatastore(inputSize,imgTrain);
%-----------------------
 
 % Ingreso de capas de la red neuronal
 conv1 = convolution2dLayer(4,16,'Stride',2,'Padding',2,'BiasLearnRateFactor',2);
 relu = reluLayer();
 maxPool = maxPooling2dLayer(2,'Stride',2);
 conv2 = convolution2dLayer(4,32,'Stride',2,'Padding',2,'BiasLearnRateFactor',2);
 relu2 = reluLayer();
 maxPool2 = maxPooling2dLayer(2,'Stride',2);
 %conv3 = convolution2dLayer(4,64,'Stride',2,'Padding',2,'BiasLearnRateFactor',2);
 %relu3 = reluLayer();
 %maxPool3 = maxPooling2dLayer(2,'Stride',2);
 fc1 = fullyConnectedLayer(num_categories,'BiasLearnRateFactor',2);
 sm1 = softmaxLayer();
 class = classificationLayer();
 
 layers = [
    imageInputLayer([128 128 3])  
    conv1
    relu    
    maxPool 
    conv2
    relu2    
    maxPool2
    %conv3
    %relu3    
    %maxPool3
    fc1
    sm1
    class];

% Modificador de opciones
opts = trainingOptions('sgdm', ... % Utiliza el descenso de gradiente estocastico conimpulso para el entrenamiento de la red
    'InitialLearnRate', 0.0001, ... % Rango inicial de aprendizaje en "0.0001"
    'ValidationData',augimgTrain, ...
    'ValidationFrequency',1, ...
    'MaxEpochs', 10, ...	% Cantidad maxima de epoca
    'MiniBatchSize', 64, ...	% Mini lote con [x] observaciones en cada iteracion
    'L2Regularization', 0.003, ...
    'Plot', 'training-progress','Verbose', true);

disp('ENTRENANDO RED...')

[net,info] = trainNetwork(augimgTrain,layers,opts);

disp('RED ENTRENADA.')
% ------------ CONSULTAR SI DESEA TOMAR FOTO ------------

consulta = 'Desea tomar una foto para el testing? (s/n) : ';
respuesta = input(consulta, 's');

if strcmp(respuesta, 's')
    % TOMAR FOTO CON LA WEBCAM Y GUARDARLA PARA EL TESTING
	cam=webcam;
	disp('LA FOTO SE TOMARA EN: ')
	for i=3:-1:1
		pause(0.5)
		disp(['		', num2str(i), '...'])
	end	   
    img=snapshot(cam);
    imwrite( img , strcat('INPUT.jpg'));
	
	disp('FOTO TOMADA CON EXITO.')
    
    %TESTEAR LA IMAGEN
    fotoTest = imageDatastore(fullfile('INPUT.jpg'),...
    'LabelSource','foldernames');
    augfotoTest = augmentedImageDatastore(inputSize,fotoTest);
    [YPred,probs] = classify(net,augfotoTest);
    
    % PORCENTAJE DE EXACTITUD DE LA RED
    accuracy = sum(YPred == fotoTest.Labels)/numel(fotoTest.Labels); 
    % ---
    subplot(1,1,1)
    I = readimage(fotoTest,1);
    imshow(I)
    label = YPred(1);
    % CALCULO DEL PORCENTAJE DE EXACTITUD DEL TESTING
    clearvars max
    [maximo,pos] = max(probs);    
    % ---
    
	% ASIGNACION DE COLOR AL TITULO DE LA IMAGEN SEGUN EL ACIERTO
    if YPred(1) == fotoTest.Labels(1) & (maximo >= 0.75)
        colorText = 'g';
        title(string(label) + ", " + num2str((100*maximo),3) + "%", 'Color', colorText);
    else
        colorText = 'r';
        title("IMAGEN NO RECONOCIDA - " + string(label) + ", " + num2str((100*maximo),3) + "%", 'Color', colorText);
    end
	clearvars cam;
else
    % USO DEL DATA STORE DE TESTEO
    
    % Clasificacion de imagenes prueba y calculo de precision de la clasificacion
    augimgTest = augmentedImageDatastore(inputSize,imgTest);
    [YPred,score] = classify(net,augimgTest);
    accuracy = sum(YPred == imgTest.Labels)/numel(imgTest.Labels); 
    
    % MUESTRA LA CANTIDAD DE IMAGENES DEL TESTEO
    [a, b] = size(imgTest.Files);
    for i = 1:(a)
        subplot(6,5,i)
        I = readimage(imgTest,i);
        imshow(I)
        title('Persona: N° ' + string(i));
    end
    %%%%%

    % Ingreso de imagen a probar
    prompt = 'Ingrese el número de imagen que desea probar: ';
    x = input(prompt);
    x = double(x);
    
    % VERIFICADOR DEL VALOR DE X
    while( x > size(imgTest.Files) & x <= 0)
        x = input(prompt);
        x = double(x);
    end
    
    % PLOTEO DE LA IMAGEN
%     subplot(1,1,1)
%     I = readimage(imgTest,x);
%     imshow(I)
    label = YPred(x);
    
    % CALCULO DEL PORCENTAJE DE EXACTITUD DEL TESTING
    clearvars max
	[maximo2,pos2] = max(score(x,:));
	
	% ASIGNACION DE COLOR AL TITULO DE LA IMAGEN SEGUN EL ACIERTO	
    if YPred(x) == imgTest.Labels(x) & (maximo2 >= 0.75)
         colorText = 'g';
%         title(string(label) + ", " + num2str((100*maximo2),3) + "%", 'Color', colorText);
        ax1 = subplot(1,2,1);
        ax2 = subplot(1,2,2);
        I = readimage(imgTest,x);
        image(ax1,I)
        title(ax1,{char(label),num2str(max(score(x,:)),2)},'Color',colorText);
        % SELECCION DEL TOP 4
        [~,idx] = sort(score,'descend');
        idx = idx(4:-1:1);
        classes = net.Layers(end).Classes;
        classNamesTop = string(classes(idx));
        scoreTop = score(x,idx);
        %MOSTRAR EL TOP 5 EN UN HISTOGRAMA
        barh(ax2,scoreTop)
        xlim(ax2,[0 1])
        title(ax2,'Top 4')
        xlabel(ax2,'Probability')
        yticklabels(ax2,classNamesTop)
        ax2.YAxisLocation = 'right';
    else
         colorText = 'r';
%         title("IMAGEN NO RECONOCIDA - " + string(label) + ", " + num2str((100*maximo2),3) + "%", 'Color', colorText);
        ax1 = subplot(1,2,1);
        ax2 = subplot(1,2,2);
        I = readimage(imgTest,x);
        image(ax1,I)
        title(ax1,{char(label),num2str(max(score(x,:)),2)},'Color',colorText);
        % SELECCION DEL TOP 4
        [~,idx] = sort(score,'descend');
        idx = idx(4:-1:1);
        classes = net.Layers(end).Classes;
        classNamesTop = string(classes(idx));
        scoreTop = score(x,idx);
        %MOSTRAR EL TOP 5 EN UN HISTOGRAMA
        barh(ax2,scoreTop)
        xlim(ax2,[0 1])
        title(ax2,'Top 4')
        xlabel(ax2,'Probability')
        yticklabels(ax2,classNamesTop)
        ax2.YAxisLocation = 'right';
    end 
end

% ------------------------------------------------------------